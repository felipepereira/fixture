import { getFixtures, getFixture } from "../src/index";
import Person from "./dto/person.dto";
import chai from 'chai';

describe("getFixtures() tests result:", () => {
  chai.should();

  it("Should return an array of objects", () => {
    const totalOfFixtures = Math.floor(Math.random() * 10) + 1;

    const fixtures = getFixtures(Person, totalOfFixtures);
    
    fixtures.should.not.be.undefined;
    fixtures.should.have.lengthOf(totalOfFixtures);

    console.table(fixtures);
  });
});

describe("getFixture() test result:", () => {
  chai.should();

  it("Should return a valid Person instance", () => {
    const fixture = getFixture(Person);
    
    fixture.should.not.be.undefined;
    fixture.should.be.instanceOf(Person);

    console.log(fixture);
  });
});