import {CompanyName, Nested} from '../../src/decorators';
import Address from './address.dto';
import { Cnpj } from "../../src/decorators/company/cnpj";

export default class Company {
    @CompanyName({locale: 'pt_BR'})
    company: string;

    @Cnpj()
    cnpj: string;

    @Nested()
    address: Address;

    constructor() {
        this.company = '';
        this.cnpj = '';
        this.address = new Address();
    }
}