import {
  City,
  Country,
  SecondaryAdress,
  State,
  Street,
  Zipcode,
  Cnpj,
  CompanyName,
  FutureDate,
  Month,
  PastDate,
  RecentDate,
  SoonDate,
  Weekday,
  Email,
  Nested,
  Cpf,
  FirstName,
  FullName,
  LastName,
  PhoneNumber,
  ArrayElement,
  Boolean,
  HexaDecimal,
  Number,
  Combine
} from "../../src/decorators";
import Company from "./company.dto";

export default class Person {
  @Combine("{{name.firstName}} {{name.lastName}}", { locale: "pt_BR" })
  titleName: string;

  @FirstName({ locale: "pt_BR" })
  firstName: string;

  @LastName({ locale: "pt_BR" })
  lastName: string;

  @ArrayElement(["2", "1"], { locale: "pt_BR" })
  elem: string;

  @Nested()
  company: Company;

  @PastDate()
  date: string;

  @Email()
  email: string;

  @Cpf()
  cpf: string;

  @Zipcode()
  zipcode: string;

  idade: number;

  constructor() {
    this.firstName = "";
    this.date = "";
    this.elem = "";
    this.titleName = "";
    this.zipcode = "";
    this.email = "";
    this.cpf = "";
    this.lastName = "";
    this.company = new Company();
    this.idade = 0;
  }
}
