import {Country, City} from '../../src/decorators/address';

export default class Address {
    @City({locale: 'pt_BR'})
    city: string;

    @Country({locale: 'pt_BR'})
    country: string;

    constructor() {
        this.city = '';
        this.country = '';
    }
}