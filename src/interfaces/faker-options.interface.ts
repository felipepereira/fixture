export default interface fakerOptions {
  locale:
    | "az"
    | "cz"
    | "de_AT"
    | "de_CH"
    | "de"
    | "en_AU"
    | "en_au_ocker"
    | "en_BORK"
    | "en_CA"
    | "en_GB"
    | "en_IE"
    | "en_IND"
    | "en"
    | "en_US"
    | "es"
    | "es_MX"
    | "fa"
    | "fr_CA"
    | "fr"
    | "ge"
    | "id_ID"
    | "it"
    | "ja"
    | "ko"
    | "nb_NO"
    | "nep"
    | "nl"
    | "pl"
    | "pt_BR"
    | "ru"
    | "sk"
    | "sv"
    | "tr"
    | "uk"
    | "vi"
    | "zh_CN"
    | "zh_TW";
}
