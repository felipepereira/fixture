import "reflect-metadata";
import { ClassType } from "class-transformer/ClassTransformer";

export * from './decorators';

export function getFixtures<FixtureType>(
  fixtureType: ClassType<FixtureType>,
  numberOfFixtures: number = 1
): FixtureType[] {
  let fixtures = [];

  for (let i = 0; i < numberOfFixtures; i++) {
    let fixture = getFixture<FixtureType>(fixtureType);

    fixtures.push(fixture);
  }

  return fixtures;
}

export function getFixture<FixtureType>(fixtureType: ClassType<FixtureType>) {
  let fixture = new fixtureType();
  const propreties = Object.getOwnPropertyNames(fixture);

  propreties.forEach(property => {
    buildFixture<FixtureType>(fixture, property);
  });
  
  return fixture;
}

function buildFixture<FixtureType>(fixture: FixtureType, property: string) {
  let fakerFuction = Reflect.getMetadata(
    "fixture",
    fixture,
    property.toString()
  );
  Object.defineProperty(fixture, property.toString(), {
    enumerable: true,
    configurable: true,
    writable: true,
    value: fakerFuction ? fakerFuction(fixture, property) : null
  });
}
