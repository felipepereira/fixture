import "reflect-metadata";
import { getFixture } from "../..";

export function Nested() {
  return Reflect.metadata("fixture", <FixtureType>(
    fixture: FixtureType,
    property: string
  ) => {
    const nestedType = Reflect.getMetadata("design:type", fixture, property);
    return getFixture(nestedType);
  });
}
