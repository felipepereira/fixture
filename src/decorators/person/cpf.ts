import FakerOptions from "../../interfaces/faker-options.interface";
import "reflect-metadata";

export function Cpf(fakerOptions?: FakerOptions) {
  return Reflect.metadata("fixture", () => {
    let firstNineDigits = "";

    for (let i = 0; i < 9; ++i) {
      firstNineDigits += String(Math.floor(Math.random() * 9));
    }

    const checker1 = calcFirstChecker(firstNineDigits);
    const generatedCPF =
      firstNineDigits +
      checker1 +
      calcSecondChecker(firstNineDigits + checker1);

    return generatedCPF;
  });
}

function calcFirstChecker(firstNineDigits: string): number {
  let sum: number = 0;

  for (let j = 0; j < 9; ++j) {
    sum += Number(firstNineDigits.toString().charAt(j)) * (10 - j);
  }

  const lastSumChecker1 = sum % 11;
  const checker1 = lastSumChecker1 < 2 ? 0 : 11 - lastSumChecker1;

  return checker1;
}

function calcSecondChecker(cpfWithChecker1: string): number {
  let sum: number = 0;

  for (let k = 0; k < 10; ++k) {
    sum += Number(cpfWithChecker1.toString().charAt(k)) * (11 - k);
  }

  const lastSumChecker2 = sum % 11;
  const checker2 = lastSumChecker2 < 2 ? 0 : 11 - lastSumChecker2;

  return checker2;
}