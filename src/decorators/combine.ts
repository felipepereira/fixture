import FakerOptions from "../interfaces/faker-options.interface";
import "reflect-metadata";
import faker from "faker";

export function Combine(combinedFakeData: string, fakerOptions?: FakerOptions) {
  return Reflect.metadata("fixture", () => {
    if (fakerOptions && fakerOptions.locale)
      faker.locale = fakerOptions.locale;

    return faker.fake(combinedFakeData);
  });
}
