# fixture.js - Generate fake data from class declaration using decorators 

![npm (scoped)](https://img.shields.io/npm/v/@felipepereira/fixture) ![npm bundle size](https://img.shields.io/bundlephobia/minzip/@felipepereira/fixture)

## Usage

### Node.js

1- In your dto class import the decorators you need to use
```typescript
    import {FirstName} from '@felipepereira/fixture'
```

2- Use the imported decorator to define which kind of fake data the class property must receive.
```typescript
    @FirstName()
    name: string;
```

3- You must initialize the property otherwise javascript will not add it to objects prototype. You can either do it in property declaration:
```typescript
    @FirstName()
    name: string = '';
```
or class contructor:
```typescript
    constructor() {
        this.name = '';
    }
```

4- Now you are able to create fake valide data for your objects  by calling getFixture or getFixtures functions with the class as parameter:
```typescript
    import {FirstName, getFixture} from '@felipepereira/fixture'

    export default class Person {
        @FirstName()
        name: string;

        constructor() {
            this.name = '';
        }
    }

    const fixture = getFixture(Person);
```
In that case fixture value would be a Person object with a random property name value. Something like:
```typescript    
    Person {
        name: 'Felipe'
    }
```
